﻿var productid = '<#861#>';
helpcontent[0] = new Array("",
"<#3927#>",
"<#3967#>",
"<#3890#>",
"<#4002#>",
"<#3880#>",
"<#3947#>",
"<#4017#>",
"<#4013#>",
"<#3942#>",
"<#3937#>",
"<#4028#><#2518#>", //11
"<#870#>",
"<#871#>",
"<#3892#><br/><#3944#>",
"<#3916#>",
"<#3929#>",
"",
"WEP-64bits: <#870#><br/>WEP-128bits: <#871#>",
"<#870#><br/><#871#>",
"<#870#><br/><#871#>",
"<#870#><br/><#871#>", //21
'<div><#683#></div><br/><img src="/images/qis/select_wireless.jpg">',
'<div><#684#></div><br/><img width="350px" src="/images/qis/security_key.png">',
"",
"<#893#>",
"<#890#>",
"Smart Connect is the feature which could automatically steer clients to the most appropriate band (2.4GHz, 5GHz-1 and 5GHz-2)." /*untranslated*/
);
if ("<% nvram_get("wl2_band"); %>".length == 0)
helpcontent[0][27] = "Smart Connect is the feature which could automatically steer clients to the most appropriate band (2.4GHz and 5GHz)."; /*untranslated*/
helpcontent[1] = new Array("",
"<#3955#>",
"<#3890#>",
"<#3972#>",
"<#4003#>",
"<#4004#>",
"<#4005#>",
"<#4006#>");
helpcontent[2] = new Array("",
"<#3872#>",
"<#3876#>",
"<#3874#>");
helpcontent[3] = new Array("",
"<#4019#>",
"<#4021#><p><a href='/Main_LogStatus_Content.asp' target='_blank'><#289#><#1540#></a></p>",
"<#4023#><p><a href='/Main_LogStatus_Content.asp' target='_blank'><#289#><#1540#></a></p>",
"<#3979#>",
"<#3995#>",
"<#3896#>",
"<#3922#>",
"<#3894#>",
"<#3986#>",
"<#4032#>",
"<#3978#>",
"<#3965#>",
"<#4037#>",
"<#4040#>",
"<#4011#>",
"<#4015#>",
"<#3960#>",
"<#3976#>",
"[n Only]: <#3989#>",
"<#4058#>",
"<#3982#>", //21
"<#3991#>",
"<#4034#>",
"<#3985#> (It's also called [Explicit Beamforming].)", /* untranslated */
"<#3994#> (It's also called [Implicit Beamforming].)", /* untranslated */
"<#3954#>",
"<#3952#>",
"<#4354#> <#3964#>",
"<#4026#>",
"<#3130#>",
"<#4031#>", //31
"<#3963#>",
"<#3964#>",
"<#3970#>"
);
helpcontent[4] = new Array("",
"<#363#>",
"<#365#>",
"<#2616#>");
helpcontent[5] = new Array("",
"<#2547#>",
"<#2557#><#2558#>",
"<#2570#>",
"<#2568#>",
"<#2560#>",
"<#2620#>",
"<#2617#>",
"<#2635#>",
"<#2562#>",
"<#364#>",
"<#2566#>", //11
"<#2567#>",
"<#2601#>",
"<#3115#>",
"<#3116#>",
"Enable Fast Leave",
"<#2593#>",
"<#1723#>");
helpcontent[6] = new Array("",
"<#3076#>",
"<#3077#>",
"<#3078#>",
"<#3079#>",
"<#3080#>",
"<#3081#>",
"<#3101#>");
helpcontent[7] = new Array("",
"<#301#>",
"<#323#>",
"<#321#>",
"<#2997#>",
"<#2996#>",
"<#2994#>",
"<#3014#>",
"<#3012#>",
"<#3020#>",
"<#2998#>",
"<#3016#>", //11
"<#319#>",
"<#305#>",
"<#307#>",
"<#1533#>",
"<#502#>",
"<#3018#>",
"<#3000#>",
"<#1532#>",
"<#2421#>",
"<#2640#>", //21
"<#2347#>",
"<#2436#>",
"<#2431#>",
"<#2429#>",
"<#2430#>",
"<#614#>",
"<#2503#>",
"<#2989#>",
"<#2990#>", //30
"<#3004#>", //31
"<#3007#>",
"<#3009#>",
"<#3011#>",
"<#3802#>",
"<#3790#>",
"<#3794#>",
"<#3796#>",
"<#3798#>",
"<#3800#>",
"<#3788#>", //41
"<#3786#>",
"<#3805#>",
"<#3807#>",
"<#3809#>",
"<#3811#>"
);
helpcontent[8] = new Array("",
"<#2110#>",
"<#2116#> Notice that this option will control http and https port access from WAN interface.", /* untranslated */
"<#2119#>",
"<#2112#>",
"<#2114#>",
"<#2067#>",
"<#2065#>");
helpcontent[9] = new Array("",
"<#2100#>",
"<#2102#>",
"<#2080#>");
helpcontent[10] = new Array("",
"<#2076#>",
"<#2078#>",
"<#2080#>",
"<#2085#>",
"<#2083#>");
helpcontent[11] = new Array("",
"<#2624#>",
"<#2628#>",
"<#2621#>",
"<#608#> <#368#>",
"<#608#> <#275#>",
"<#1896#>",
"<#2632#>",
"<#2634#>",
"For destination IP address, you can:<br/>(a) enter a specific IP address, such as \"192.168.1.2\"<br/>(b) enter IP addresses within one subnet or within the same IP pool, such as \"192.168.1.0/24\"", /* untranslated */
"<#769#>",
"<#3552#>",
"If there is no client connection for more than 1 minute, the PLC will enter sleep mode (power saving). The PLC will not wake up until the client connects. (It takes about ten seconds to wake up the PLC)", /* untranslated */
"Enable Login CAPTCHA is to ensure only human users to pass through and prevent brute force login attack.", /* untranslated */
"<#2155#>");
helpcontent[12] = new Array("",
"<#2249#>",
"<#3030#>",
"<#3032#>",
"<#3034#>");
helpcontent[13] = new Array("",
"<#4042#>",
"<#4048#>",
"<#4050#>",
"<#3974#>",
"<#4046#>");
helpcontent[14] = new Array("",
"<#3530#>",
"");
helpcontent[15] = new Array("",
"", /*<#986#>*/
"<#993#><p><a href='../Advanced_AiDisk_ftp.asp' target='_parent' hidefocus='true'><#2812#></a></p><!--span style='color:red'><#994#></span-->",
"<#996#>",
"<#998#>");
helpcontent[16] = new Array("",
"<#1936#><p><#1937#> <a href='/QoS_EZQoS.asp'><#1525#></a></p>");
helpcontent[17] = new Array("",
"<#2526#>",
"<#2527#>",
"<#2315#>",
"<#2509#>",
"<#2510#>",
"<#2511#>",
"<#3209#>",
"<#3207#>",
"<#3199#>");
helpcontent[18] = new Array("",
"<#2097#>",
"<#2966#>",
"<#2414#>",
"<#2966#>",
"<#3576#>",
"<#3266#>");
helpcontent[19] = new Array("",
"<#3180#>",
"<#3186#>",
"<#3190#>",
"<#3181#>"
);
helpcontent[20] = new Array("",
'<#1920#>',
"<#2703#>",
"<#2687#>",
"<#1534#>",
"<#1932#>",
"<#1934#>",
"<#163#>");
helpcontent[21] = new Array("",
"<#2333#>",
"<#2338#>",
"<#2339#>",
"HSDPA<#3014#>",
"HSDPA<#3012#>",
"<#305#>",
"<#307#>",
"<#2334#>",
"<#2323#>",
"<#2326#>",
"<#2343#>",
"<#2336#>",
"<#2342#>");
helpcontent[22] = new Array("",
"<#456#>",
"<#456#>",
"<#449#>");
helpcontent[23] = new Array("",
"",
"",
"<#3291#>",
"<#3293#>",
"<#3292#>");
helpcontent[24] = new Array("",
"<#305#>", //7,13
"<#307#>", //7,14
"<#319#>",//7,12
"<#321#>",//7,3
"<#363#>", //4,1
"<#365#>", //4,2
"<#368#>", //11,4
"<#614#>", //7,27
"<#683#>", //0,22
"<#684#>"); //0,23
helpcontent[25] = new Array("",
"<#1835#>",
"<#1834#>",
"<#1821#>",
"This item allows you to tweak the target SNR Margin of VDSL. For instance with a downstream SNR Margin at 8dB, you could set to 7dB or lower value to maximize the downstream performance, 2dB (Max.performance) but please note that the lower the value, DSL modem router will be weaker to defend the line noise, thus sync loss might occur, so please adjust with proper value. However, if your VDSL connection is unstable or not able to establish a connection, for this case then set to a higher value such as 9dB ~ 30dB.",
"This item allows you to tweak the Tx Power of VDSL. Reduce Tx Power(-1 dB ~ -7 dB) would increase the downstream performance(reduce more Tx Power leads to higher downstream data rate), but will impact upstream and vice versa.",
"This item configures Rx AGC(Auto Gain Control) GAIN for VDSL, if tweak the Stability Adjustment (VDSL) setting still could not get desired downstream speed, then could try to set Rx AGC GAIN Adjustment to High Performance mode. However if your VDSL connection is unstable and has some CRC then could set to Stable mode.",
"This item allows you to control whether to Enable/Disable UPBO(Upstream Power Back Off) for VDSL. DSLAM could use UPBO to reduce the Tx Power of your xDSL modem router, in some cases abnormal UPBO control from DSLAM could leads to sync up issue(such as not enough Tx Power to sync with minimum rate). Thus with this feature now you could disable UPBO and will not get affected by DSLAM setting.",
"This item configures Profile to be used for VDSL connection, default is 30a multi mode. However in order to work around the non-standard 30a multi mode VDSL DSLAM sync issue, which deployed by some of Germany ISP, set to 17a multi mode might be needed in order for the VDSL line to sync up. For users of other countries, there is no need to change this setting.",
"Apply special settings for specific country. With this option, the Stability Adjustment for ADSL is properly set according to the selected country.",
"This item allows system to monitor the DSL line, designed to maintain stability of the line. Based on current line condition necessary changes will be adopted.", //10
"This feature allows system to capture diagnostic DSL debug log in the background, duration depends on the \"Diagnostic debug log capture duration\" option, after capture completed debug log would be transmitted automatically to ASUS Support Team for analysis.",
"The G.INP stands for Impulse Noise Protection. It works on ADSL2, ADSL2+, and VDSL2 only. It is enabled to provide enhanced protection against impulse noise or to increase the efficiency of providing impulse noise protection. If your DSLAM does not support it, please disable it.",
"This item configures Rx AGC(Auto Gain Control) GAIN for ADSL, if tweak the Stability Adjustment (ADSL) setting still could not get desired downstream speed, then could try to set Rx AGC GAIN Adjustment to High Performance mode. However if your ADSL connection is unstable and has some CRC then could set to Stable mode.",
"This item supports G.vector. With G.vector crosstalk among the signals in the same cable could be canceled, such as far-end crosstalk (FEXT). Which would significantly improve Signal-to-Noise Ratio (SNR) that leads to higher achievable bit rates. However CO must deploy Vectored VDSL2 DSLAM in order for this feature to work. If you find it doesn't work well or you know the G.vector of your ISP is non-standard, please enable both of this option and Non-standard G.vector.",
"This item supports Non-standard G.vector for specific countries. Please note that if your G.vector is standard, please do not enable this option for optimized performance.",
"This command is helpful for some impulse noise environment to enhance line stability.",
"Enhancing the detection of impulse noise may improve certain lines with frequent noise fluctuations. If your DSL line supports G.INP and your connection is often unstable, then you can try to enable it.",
"This item allows you to tweak the target SNR Margin. You could set to negative value to maximize the downstream performance. But please note that the lower the value, DSL modem router will be weaker to defend the line noise. Sync loss might occur. So please adjust with proper value. However, if your xDSL connection is unstable or not able to establish a connection, setting to a postive value.",
""
);
helpcontent[26] = new Array("",
"<#1868#>",
"<#1877#>",
"<#1876#>",
"<#1867#>",
"<#1878#>",
"<#1852#>");
helpcontent[27] = new Array("",
"<#4086#>: <#4087#><br>" +
"<#4088#>: <#4089#><br>" +
"<#4090#>: <#4091#>");
if('<% nvram_default_get("lan_ipaddr"); %>' != "192.168.1.1"){
helpcontent[4][1] = "<#363#>".replace("192.168.1.1", '<% nvram_default_get("lan_ipaddr"); %>');
helpcontent[24][5] = "<#363#>".replace("192.168.1.1", '<% nvram_default_get("lan_ipaddr"); %>');
}
helpcontent[28] = new Array("");
helpcontent[29] = new Array("",
"<#2827#>",
"<#2816#>");
helpcontent[30] = new Array("",
"Send alert before monthly alert is reached",/*untranslated*/
"Cut-off internet if monthly limit is reached");/*untranslated*/
helpcontent[31] = new Array("",
"The WiFi and authentication session will be disconnected if user does not process within the defined Idle time.",/*untranslated*/
"The WiFi and Internet connection will be disconnected after a specified period of inactivity.",/*untranslated*/
"The WiFi and Internet connection will be disconnected if the defined Internet session time expired.",/*untranslated*/
"The NAS ID is string on RADIUS attribute 32, which allows captive portal to send authentication request to RADIUS server for applying different policy of user group.Then, the RADIUS server can send a customized authentication response base on the received NAS ID for the captive portal. To enable the features, you must configure NAS ID policy on RADIUS server correspondingly.",/*untranslated*/
"Enter a whitelist URL, also known as walled garden.\nNote: Guest user can fetch the whitelist web content under the domain name without any authentication."/*untranslated*/
);
helpcontent[32] = new Array("",
"<#4233#>",
"Enable this option allows VPN clients to access the subnet of your LAN",/*untranslated*/
"Enable this option allows VPN clients use the Internet from your router instead of the one at their location.",/*untranslated*/
"Virtual network device type. TUN devices encapsulate IPv4 or IPv6 (OSI Layer 3) while TAP devices encapsulate Ethernet 802.3 (OSI Layer 2).",/*untranslated*/
"Choose the communicating protocol with remote host.",/*untranslated*/
"Set the port number to bind. The current default of 1194 represents the official IANA port number assignment for OpenVPN.",/*untranslated*/
"<b>TLS</b>: OpenVPN runs in server mode and SSL/TLS authentication will be used;<br> <b>Static Key</b>: OpenVPN runs in P2P mode.",/*untranslated*/
"The bits size of automatically generated certificate.",/*untranslated*/
"Use username/password only allows client connect to server without certification and authentication by username/password. Be aware that using this directive is less secure than requiring certificates.",/*untranslated*/
"Add an additional layer of HMAC authentication on top of the TLS control channel to protect against DoS attacks. An OpenVPN static key will be used.",/*untranslated*/ //10
"This directive will set up an OpenVPN server which will allocate addresses to clients out of the given network/netmask. The server itself will take the \".1\" address of the given network for use as the server-side endpoint of the local TUN/TAP interface.",/*untranslated*/
"The IP address of the local and remote VPN endpoint in p2p mode.",/*untranslated*/
"<b><#194#></b>: Use LAN DHCP server to allocate IP address;<br> <b><#193#></b>: Allocate IP address from the Address Pool",/*untranslated*/
"The first address and the last address in the pool to be assigned to clients.",/*untranslated*/
"Response the DNS query from clients.",/*untranslated*/
"In server mode, provide DNS information to clients.",/*untranslated*/
"The cipher algorithm to encrypt packets in transmission. AES-128-CBC is recommendation.",/*untranslated*/
"Use fast LZO compression. It may add up to 1 byte per packet for incompressible data.",/*untranslated*/
"This option can be used on both the client and server, and whichever uses the lower value will be the one to trigger the renegotiation. Renegotiate data channel key after n seconds (default=3600), 0 to disable.",/*untranslated*/
"When this option is enabled, each client can view the other clients which are currently connected. Otherwise, each client will only see the server.",/*untranslated*/ //20
"Only the client in the \"Allowed Clients\" table could be authenticated.",/*untranslated*/
"The Username / Common Name(CN) of client certificate.<br> If setting authenticated by username / password only, this field should be the username in the \"Username and Password\" table.",/*untranslated*/
"The Network Address of a subnet to generate an internal route to a specific client. This specific client should own this subnet.",/*untranslated*/
"The Network Mask of a subnet to generate an internal route to a specific client. This specific client should own this subnet.",/*untranslated*/
" If you would like other clients to be able to this specific client's subnet, choose Yes and Enable \"Allow Client <-> Client\".",/*untranslated*/
"The message digest algorithm which is used to authenticate packets with HMAC. HMAC usually adds 16 or 20 bytes per packet.",/*untranslated*/
"When you would restore or replace router, you can keep original certification of OpenVPN server via \"Export Current Certification\".",/*untranslated*/
"You can shift original certification of OpenVPN server from your other one or the old ASUS router, rather than create a new one and ask all clients to setup OpenVPN profile again."/*untranslated*/
);
helpcontent[33] = new Array("",
"<#3706#>",
"<#3724#>"
);
helpcontent[34] = new Array("",
"This feature allows system to capture diagnostic System debug log in the background, duration depends on the Diagnostic debug log capture duration option, depends on the option selected, system might transmit single debug log automatically to ASUS Support Team for analysis after capture completed or transmit multiple debug logs over a period of time. Click on the yellow System icon could cancel the debug log capture.",/*untranslated*/
"<#1945#>",
"<#2023#>"
);
helpcontent[35] = new Array("",
"<#2782#>",
"<#2797#>",
"<#2794#>",
"<#2780#>");

