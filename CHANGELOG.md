## 3.0.0.4.386.69061

- Fixed v6plus related issues and added support for OCN.

## 3.0.0.4.386.69021

### Added

-iPhone/Android USB auto backup WAN allows you to connect your phone to the router’s USB port and use it as an internet source. Please refer to https://www.asus.com/support/FAQ/1050074/

-DDNS transfer allows you to transfer your ASUS DDNS hostname from your original router to the new one. Please refer to https://www.asus.com/support/FAQ/1048684/

### Security updates:
-Allowed binding DDNS to a user's account to reduce the risk of MITM attacks
-Fixed the cfg server vulnerability.
-Fixed lighttpd vulnerability, CVE-2023-35720.
-Fixed several curl vulnerabilities including CVE-2023-28322, CVE-2023-28321, and CVE-2023-28319.
-Fixed OpenSSL vulnerability, CVE-2023-0464.
-Patched several command injection vulnerabilities.
-Upgraded sqlighte and resolved CVE-2020-11656 / CVE-2019-19646 / CVE-2019-8457 / CVE-2020-11655 / CVE-2018-20505 / CVE-2019-16168 / CVE-2019-19645 / CVE-2020-13435 / CVE-2020-13631 / CVE-2020-13434

## 3.0.0.4.386.68691

1. Fixed an issue with changing passwords.
2. Fixed bandwidth errors when 5GHz channel is fixed at 116.
3. Improved security by upgrading to SHA256.
4. Added support for disabling 11b on 2.4GHz band
5. Improved DDNS functionality and added HE.NET
6. Improved smart connect function.
7. Added LED on/off feature as Aimesh node.
8. Optimized AiMesh Topology page.
9. Improved Aimesh stability.

### 3.0.0.4.386.68526

1.Improved JP SKU initial setup process.
2.Improved system stability.
3.Fixed multi-language related GUI bugs.
4.Improved AiMesh stability.
5.Fixed IPSec VPN related performance issue. 

### 3.0.0.4.386.68383

1.Improved system stability.
2.Fixed firmware upgrade issue in the latest step of the initial setup process.
3.Fixed web user interface display bugs after disabling wireless.

### 3.0.0.4.386.68129

1. Fixed httpd error when turning off the radio.
2. Fixed connection issue when SSID containing space and UTF8 under AiMesh or Repeater mode.
3. Fixed IPv6 manual DNS setting bugs.
4. Fixed UI display error of USB types.
5. Fixed AiMesh related issue.
6. Added new entrance for web GUI http://www.asusrouter.com

### 3.0.0.4.386.67508

1. Supported AiMesh function.
2. Fixed IPTV bugs.
3. Fixed WPA2 PSK/WPA3 PSK mix mode issues.
4. Improved system stability.
